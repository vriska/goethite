#![no_std]
#![no_main]

use core::panic::PanicInfo;
use core::sync::atomic::{AtomicPtr, Ordering};

#[repr(packed)]
pub struct Version(u8, u8);

#[repr(packed)]
pub struct Header {
    pub header: [u8; 8],
    pub struct_version: Version,
    pub sdk_version: Version,
    pub process_version: Version,
    pub load_size: u16,
    pub offset: u32,
    pub crc: u32,
    pub name: [u8; 32],
    pub company: [u8; 32],
    pub icon_resource_id: u32,
    pub sym_table_addr: u32,
    pub flags: u32,
    pub num_reloc_entries: u32,
    pub uuid: [u8; 16],
    pub resource_crc: u32,
    pub resource_timestamp: u32,
    pub virtual_size: u16,
}

#[used]
#[link_section = ".pbl_header"]
pub static PEBBLE_APP_INFO: Header = Header {
    header: *b"PBLAPP\0\0",
    struct_version: Version(0x10, 0x0),
    sdk_version: Version(0x5, 0x56),
    process_version: Version(1, 0),
    load_size: 0xb6b6,
    offset: 0xb6b6b6b6,
    crc: 0xb6b6b6b6,
    name: *b"goethite\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0",
    company: *b"leo60228\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0",
    icon_resource_id: 0,
    sym_table_addr: 0xa7a7a7a7,
    flags: 0x2 << 6,
    num_reloc_entries: 0xdeadcafe,
    uuid: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
    resource_crc: 0,
    resource_timestamp: 0,
    virtual_size: 0xb6b6,
};

#[export_name = "pbl_table_addr"]
pub static mut JUMP_TABLE: usize = 0xdeadbeef;

pub fn jump_table() -> *const usize {
    unsafe { JUMP_TABLE as *const _ }
}

unsafe fn get_pbl_func<T: Copy>(idx: usize) -> T {
    let func_ptr = jump_table().wrapping_add(idx / 4) as *const T;
    *func_ptr
}

const APP_EVENT_LOOP: usize = 0x7c;
const WINDOW_CREATE: usize = 0x43c;
const WINDOW_SET_WINDOW_HANDLERS: usize = 0x468;
const WINDOW_STACK_PUSH: usize = 0x47c;
const WINDOW_DESTROY: usize = 0x440;
const WINDOW_GET_ROOT_LAYER: usize = 0x44c;
const LAYER_GET_BOUNDS: usize = 0x238;
const TEXT_LAYER_CREATE: usize = 0x738;
const TEXT_LAYER_SET_TEXT: usize = 0x75c;
const TEXT_LAYER_SET_TEXT_ALIGNMENT: usize = 0x760;
const TEXT_LAYER_GET_LAYER: usize = 0x744;
const LAYER_ADD_CHILD: usize = 0x228;
const TEXT_LAYER_DESTROY: usize = 0x73c;

#[repr(C)]
struct WindowHandlers {
    pub load: Option<unsafe extern "C" fn(*mut ())>,
    pub appear: Option<unsafe extern "C" fn(*mut ())>,
    pub disappear: Option<unsafe extern "C" fn(*mut ())>,
    pub unload: Option<unsafe extern "C" fn(*mut ())>,
}

#[repr(C)]
struct GPoint {
    pub x: i16,
    pub y: i16,
}

#[repr(C)]
struct GSize {
    pub w: i16,
    pub h: i16,
}

#[repr(C)]
struct GRect {
    pub origin: GPoint,
    pub size: GSize,
}

#[repr(C)]
pub enum GTextAlignment {
    Left,
    Center,
    Right,
}

static TEXT_LAYER: AtomicPtr<()> = AtomicPtr::new(core::ptr::null_mut());

unsafe extern "C" fn window_load(window: *mut ()) {
    let window_layer =
        get_pbl_func::<extern "C" fn(*mut ()) -> *mut ()>(WINDOW_GET_ROOT_LAYER)(window);
    let bounds = get_pbl_func::<extern "C" fn(*mut ()) -> GRect>(LAYER_GET_BOUNDS)(window_layer);

    let text_layer = get_pbl_func::<extern "C" fn(GRect) -> *mut ()>(TEXT_LAYER_CREATE)(GRect {
        origin: GPoint { x: 0, y: 72 },
        size: GSize {
            w: bounds.size.w,
            h: 20,
        },
    });

    get_pbl_func::<extern "C" fn(*mut (), *const u8)>(TEXT_LAYER_SET_TEXT)(
        text_layer,
        b"Hello from Rust!\0".as_ptr(),
    );
    get_pbl_func::<extern "C" fn(*mut (), GTextAlignment)>(TEXT_LAYER_SET_TEXT_ALIGNMENT)(
        text_layer,
        GTextAlignment::Center,
    );

    let text_layer_layer =
        get_pbl_func::<extern "C" fn(*mut ()) -> *mut ()>(TEXT_LAYER_GET_LAYER)(text_layer);

    get_pbl_func::<extern "C" fn(*mut (), *mut ())>(LAYER_ADD_CHILD)(
        window_layer,
        text_layer_layer,
    );

    TEXT_LAYER.store(text_layer, Ordering::SeqCst);
}

unsafe extern "C" fn window_unload(_window: *mut ()) {
    let text_layer = TEXT_LAYER.load(Ordering::SeqCst);
    get_pbl_func::<extern "C" fn(*mut ())>(TEXT_LAYER_DESTROY)(text_layer);
}

#[no_mangle]
pub extern "C" fn main() -> i32 {
    unsafe {
        let window = get_pbl_func::<extern "C" fn() -> *mut ()>(WINDOW_CREATE)();
        get_pbl_func::<extern "C" fn(*mut (), WindowHandlers)>(WINDOW_SET_WINDOW_HANDLERS)(
            window,
            WindowHandlers {
                load: Some(window_load),
                appear: None,
                disappear: None,
                unload: Some(window_unload),
            },
        );
        get_pbl_func::<extern "C" fn(*mut (), bool)>(WINDOW_STACK_PUSH)(window, true);

        get_pbl_func::<extern "C" fn()>(APP_EVENT_LOOP)();

        get_pbl_func::<extern "C" fn(*mut ())>(WINDOW_DESTROY)(window);
    }

    0
}

#[panic_handler]
fn panic(_panic: &PanicInfo<'_>) -> ! {
    loop {}
}
